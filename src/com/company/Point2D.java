package com.company;

public class Point2D {
    private double x;
    private double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "x: " + this.x + " y: " + this.y;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }
}
