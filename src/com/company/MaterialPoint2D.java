package com.company;

public class MaterialPoint2D extends Point2D {
    private double mass;

    public MaterialPoint2D(double x, double y, double mass) {
        super(x ,y);
        this.mass = mass;
    }

    @Override
    public String toString() {
        return "x: " + this.getX() + " y: " + this.getY() + " mass: " + this.mass;
    }

    public double getMass() {
        return mass;
    }
}
