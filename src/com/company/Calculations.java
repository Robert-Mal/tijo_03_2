package com.company;

public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[ ] points) {
        double x = 0, y =0;
        for (Point2D point: points) {
            x += point.getX();
            y += point.getY();
        }
        int length = points.length;

        return new Point2D(x / length, y / length);
    }

    public static MaterialPoint2D positionCenterOfMass(MaterialPoint2D[ ] materialPoints2D) {
        double x = 0, y = 0, mass = 0;
        for (MaterialPoint2D materialPoint2D: materialPoints2D) {
            x += materialPoint2D.getX() * materialPoint2D.getMass();
            y += materialPoint2D.getY() * materialPoint2D.getMass();
            mass += materialPoint2D.getMass();
        }

        return new MaterialPoint2D(x / mass, y / mass, mass);
    }

}
